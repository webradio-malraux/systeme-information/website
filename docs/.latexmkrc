$pdflatex = 'pdflatex --shell-escape %O %S';
$recorder = 1;
$hash_calc_ignore_pattern{'pdf'} = '.*';
add_cus_dep('glo', 'gls', 0, 'makeglo2gls');
sub makeglo2gls {
    system("makeindex -s '$_[0]'.ist -t '$_[0]'.glg -o '$_[0]'.gls '$_[0]'.glo");
}
add_cus_dep('acn', 'acr', 0, 'makeacn2acr');
sub makeacn2acr {
    system("makeindex -s '$_[0]'.ist -t '$_[0]'.alg -o '$_[0]'.acr '$_[0]'.acn");
}
add_cus_dep('puml', 'pdf', 0, 'plantuml');
sub plantuml {
    system("plantuml -tsvg '$_[0]'.puml && inkscape --without-gui --export-pdf='$_[0]'.pdf --file='$_[0]'.svg");
}
