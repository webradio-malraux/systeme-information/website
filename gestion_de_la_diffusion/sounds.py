# -*- coding: utf-8 -*-

"""
Utility functions for handling sounds.
Requires audiotools as you might imagine.
"""

import audiotools
import logging
from django.core.files import File

LOG = logging.getLogger(__name__)


class SoundFile(File):
    """
    Un mixin à utiliser aux côtés de django.core.files.base.File, fournissant
    des fonctionnalités spécifiques à la manipulation de fichiers sonores.
    """

    @property
    def duree(self):
        return self._get_sound_duree()

    def _get_sound_duree(self):
        if not hasattr(self, '_duree_cache'):
            self._duree_cache = get_sound_duree(self.path)
        return self._duree_cache

    @property
    def metadata(self):
        return self._get_sound_metadata()

    def _get_sound_metadata(self):
        if not hasattr(self, '_metadata_cache'):
            self._metadata_cache = get_sound_metadata(self.path)
        return self._metadata_cache


def get_sound_duree(file_path):
    """
    Retourne la durée, exprimée en secondes, du fichier sonore
    dont le chemin est fourni en paramètre.
    """

    try:
        LOG.debug("Recherche de la durée de {!r}".format(file_path))
        f = audiotools.open(file_path)
        frac_length = f.seconds_length()
        return frac_length.numerator // frac_length.denominator
    except:
        return 0


def get_sound_metadata(file_path):
    """
    Retourne le 'audiotools.MetaData' du fichier sonore dont le
    chemin est fourni en paramètre.
    """

    try:
        LOG.debug("Recherche des metadata de {!r}".format(file_path))
        f = audiotools.open(file_path)
        metadata = f.get_metadata()
        return metadata
    except:
        return audiotools.MetaData()
