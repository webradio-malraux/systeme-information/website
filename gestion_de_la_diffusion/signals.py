# -*- coding: utf-8 -*-

"""
Définition des signaux de l'application.
"""

import os
import logging
import audiotools
from django.utils import timezone
from website import settings
from django.db.models.signals import (post_save,
                                      pre_delete,
                                      post_delete)
from gestion_de_la_diffusion.models import (Editeur,
                                            Licence,
                                            ObjetSonore,
                                            Ordonner,
                                            Programmation)
from gestion_des_musiques.models import (Album,
                                         Artiste,
                                         Genre,
                                         Piste)
from gestion_des_emissions.models import (Emission,
                                          Episode,
                                          Intervenant,
                                          Intervention,
                                          Role)
from .exceptions import LockedError

LOG = logging.getLogger(__name__)


class PathInfos():
    """
    Ensemble des noms de fichiers et chemins utilisés pour enregistrer
    un fichier sonore dans le système de fichier :
        - diffusable_path :
          Chemin du fichier audio du `Diffusable` relatif
          au répertoire "musique/" ou "emission/".
          --> "musique/nomEditeur/nomArtiste/nomAlbum/"
              "emission/nomEditeur/nomEmission/"
        - diffusable_name :
          Nom du fichier audio du `Diffusable` sans extension
          (Pour une `Piste`, le numéro est écrit avec deux chiffres).
          --> "03.titrePiste[2018, acronymeLicence]"
              "titreEpisode[2018, acronymeLicence]"
        - save_path :
          Chemin du répertoire de sauvegarde du fichier audio
          du `Diffusable` relatif à MEDIA_ROOT.
          --> "AUDIO_DIR/diffusable_path"
        - save_filename :
          Chemin de sauvegarde du fichier audio du `Diffusable` relatif
          à MEDIA_ROOT avec le type en tant qu'extension du fichier.
          --> "save_path/diffusable_name.mp3"
        - root_save_path :
          Chemin du répertoire de sauvegarde du fichier audio du `Diffusable`.
          --> "/MEDIA_ROOT/save_path"
        - root_save_filename :
          Chemin de sauvegarde complet du fichier audio du `Diffusable`.
          --> "MEDIA_ROOT/save_filename"
    """
    def __init__(self,
                 diffusable_path=None,
                 diffusable_name=None,
                 save_path=None,
                 save_filename=None,
                 root_save_path=None,
                 root_save_filename=None):
        self.diffusable_path = diffusable_path
        self.diffusable_name = diffusable_name
        self.save_path = save_path
        self.save_filename = save_filename
        self.root_save_path = root_save_path
        self.root_save_filename = root_save_filename


def est_un_ObjetSonore(instance):
    """
    Vérifie que l'instance hérite bien de ObjetSonore.
    """
    return ObjetSonore in type(instance).__bases__


def creation_des_metadata(instance):
    """
    Permet de définir les Metadata d'une instance en fonction des
    éléments stockés en BDD.

    instance: doit être un objet dérivé de ObjetSonore.

    Retourne : un objet audiotools.MetaData.
    """
    LOG.debug("# Création des Metadata en fonction du SI")
    metadata = audiotools.MetaData()
    if est_un_ObjetSonore(instance):
        classe = instance.__class__.__name__
        metadata.track_name = str(instance.titre)
        metadata.track_number = 0
        metadata.date = str(instance.date_publication)
        metadata.year = str(instance.date_publication.year)
        metadata.copyright = str(instance.licence)
        metadata.publisher = str(instance.editeur)

        if classe == "Piste":
            metadata.track_number = instance.position
            metadata.album_name = str(instance.album)
            metadata.artist_name = str(instance.artiste)
            metadata.media = str(instance.provenance.medium)
        elif classe == "Episode":
            metadata.album_name = str(instance.emission)
            metadata.media = settings.PROVENANCE_INTERNE
            # Recherche des auteurs de l'`Episode`
            interventions_auteur = instance.intervention_set.filter(
                                       role__name="Auteur").order_by(
                                           "intervenant__last_name")
            auteurs = [intervention.intervenant.get_full_name()
                       for intervention in interventions_auteur]
            metadata.artist_name = ', '.join(auteurs)
            # Recherche des encadrants de l'`Episode`
            interventions_encadrant = instance.intervention_set.filter(
                                          role__name="Encadrant").order_by(
                                              "intervenant__last_name")
            encadrants = [intervention.intervenant.get_full_name()
                          for intervention in interventions_encadrant]
            metadata.conductor_name = ', '.join(encadrants)
            # Recherche des autres participants à l'`Episode`
            interventions_autre = instance.intervention_set.exclude(
                                      role__name="Auteur").exclude(
                                          role__name="Encadrant").order_by(
                                              "intervenant__last_name")
            participants = ["{nom} ({role})".format(
                                nom=intervention.intervenant.get_full_name(),
                                role=intervention.role.name)
                            for intervention in interventions_autre]
            metadata.performer_name = ', '.join(participants)

    return metadata


def creation_chemins_stockage(instance, metadata):
    """
    Création du chemins de stockage du fichier basé sur ses MetaData.
    """
    LOG.debug("# Création des chemins de stockage")
    chemins = PathInfos()
    if est_un_ObjetSonore(instance):
        classe = instance.__class__.__name__
        if classe == "Piste":
            # Ex : chemins.diffusable_path =
            # "musique/Editeur/Artiste/Album/"
            chemins.diffusable_path = audiotools.AudioFile.track_name(
                    "",
                    metadata,
                    "musique/%(publisher)s/%(artist_name)s/%(album_name)s/")

            # Ex : chemins.diffusable_name =
            # "03.Piste[2018, Licence]"
            chemins.diffusable_name = audiotools.AudioFile.track_name(
                    '',
                    metadata,
                    "%(track_number)2.2d.%(track_name)s"
                    "[%(year)s, %(copyright)s]")
        elif classe == "Episode":
            # Ex : diffusable_path =
            # "emission/Editeur/Emission/"
            chemins.diffusable_path = audiotools.AudioFile.track_name(
                    "",
                    metadata,
                    "emission/%(publisher)s/%(album_name)s/")

            # Ex : diffusable_name =
            # "Episode[2018, Licence]"
            chemins.diffusable_name = audiotools.AudioFile.track_name(
                    '',
                    metadata,
                    "%(track_name)s[%(year)s, %(copyright)s]")

        # Ex : save_path =
        # "AUDIO_DIR/musique/Editeur/Artiste/Album/"
        chemins.save_path = os.path.join(
                settings.AUDIO_DIR,
                chemins.diffusable_path)
        LOG.debug("   save_path: {chemin}".format(chemin=chemins.save_path))

        # Ex : save_filename =
        # "AUDIO_DIR/musique/Editeur/Artiste/Album/
        #                                     03.Piste[année, Licence].mp3"
        chemins.save_filename = os.path.join(
                chemins.save_path,
                "{nom}.{ext}".format(nom=chemins.diffusable_name,
                                     ext=settings.AUDIO_CONFIG['type']))
        LOG.debug("   save_filename: {chemin}"
                  .format(chemin=chemins.save_filename))

        # Ex : root_save_path =
        # "/MEDIA_ROOT/AUDIO_DIR/musique/Editeur/Artiste/Album/"
        chemins.root_save_path = os.path.join(
                settings.MEDIA_ROOT,
                chemins.save_path)
        LOG.debug("   root_save_path: {chemin}"
                  .format(chemin=chemins.root_save_path))

        # Ex : root_save_filename =
        # "/MEDIA_ROOT/AUDIO_DIR/musique/Editeur/Artiste/
        #                                   Album/01.Piste[année, Licence].mp3"
        chemins.root_save_filename = os.path.join(
                settings.MEDIA_ROOT,
                chemins.save_filename)
        LOG.debug("   root_save_filename: {chemin}"
                  .format(chemin=chemins.root_save_filename))

        LOG.info("# Création du répertoire: {chemin}"
                 .format(chemin=chemins.root_save_path))
        os.makedirs(chemins.root_save_path, exist_ok=True)
    return chemins


def save_metadata(sender, verif_prog=False, **kwargs):
    """
    Sauvegarde les MetaData dans le fichier de l'`ObjetSonore`.
    Si l'argument verif_prog vaut True et que l'instance est un fichier sonore,
    on vérifie qu'il n'est pas programmé avant de l'enregistrer.
    """
    LOG.info("# Appliquer les Metadata sur le fichier associé à l'ObjetSonore")
    if 'instance' in kwargs:
        instance = kwargs['instance']
        print("XXXsave_metadata de", str(instance), "XXX")
        if est_un_ObjetSonore(instance):
            if not verif_prog:
                instance.verification_programmation()
            try:
                if instance.fichier_disponible():
                    ancien_chemin = instance.fichier.path
                    metadata = creation_des_metadata(instance)
                    final_audio_file = audiotools.open(ancien_chemin)
                    metadata_existantes = final_audio_file.get_metadata()
                    if metadata != metadata_existantes:
                        final_audio_file.set_metadata(metadata)
                        LOG.debug(str(metadata))
                    chemins = creation_chemins_stockage(instance, metadata)

                    if ancien_chemin != chemins.root_save_filename:
                        # Cas où le chemin du fichier vient d'être changé ou
                        # qu'il vient d'être ajouté pour la première fois
                        length = final_audio_file.seconds_length()
                        instance.duree = length.numerator // length.denominator
                        instance.fichier = chemins.save_filename
                        instance.objetsonore_ptr.save()
                        os.renames(ancien_chemin, chemins.root_save_filename)
            except ValueError:
                LOG.warning("{!r} ne possède pas de fichier associé"
                            .format(instance))

        else:
            LOG.info("L'instance n'est pas un ObjetSonore : {!r}"
                     .format(instance))
            objets_sonores_concernes = []
            # MetaData dans le cadre de la gestion des `Emission`
            if sender is Emission:
                # Modifie les MetaData de chacun des `Episode`
                # associés à l'`Emission` modifiée
                if instance.episode_set_emission.exists():
                    objets_sonores_concernes.extend(
                            instance.episode_set_emission.all())
            elif sender in (Intervenant, Role):
                # Modifie les MetaData de chacun des `Episode` pour lequel
                # l'`Intervenant` ou le `Role` modifié effectue
                # une `Intervention`
                if instance.intervention_set.exists():
                    objets_sonores_concernes.extend(
                            instance.intervention_set.all())
            elif sender is Intervention:
                # Modifie les MetaData de l'`Episode` associé à
                # l'`Intervention` modifiée
                objets_sonores_concernes.extend((instance.episode,))
            # MetaData dans le cadre de la gestion des musiques
            elif sender in (Album, Artiste, Genre):
                # Modifie les MetaData de chacune des `Piste` pour laquelle
                # l'`Album`, l'`Artiste` ou le `Genre` modifié est associé
                if instance.piste_set.exists():
                    objets_sonores_concernes.extend(instance.piste_set.all())
            # MetaData dans le cadre de la gestion de la diffusion
            elif sender is Editeur:
                for cls in (Episode, Piste):
                    objets_sonores_concernes.extend(
                            cls.objects.filter(editeur=instance))
            elif sender is Licence:
                for cls in (Episode, Piste):
                    objets_sonores_concernes.extend(
                            cls.objects.filter(licence=instance))
            # Appel à la sauvegarde de tous les `ObjetSonore` accumulés
            # Si l'instance n'est pas un `ObjetSonore`, alors les `ObjetSonore
            # qui lui sont associés ne sont pas passé par la vérification
            # d'une éventuelle `Programmation`
            verif_prog = True
            for objet in objets_sonores_concernes:
                cls = objet.__class__
                save_metadata(sender=cls, instance=objet, verif_prog=True)


def pre_save_me(sender, **kwargs):
    """
    Fonction générique de debug pour tracer les appels.
    """
    instance = kwargs['instance']
    classe = instance.__class__.__name__
    LOG.debug("{}.pre_save()".format(classe))


def post_save_me(sender, **kwargs):
    """
    Fonction générique de debug pour tracer les appels.
    """
    instance = kwargs['instance']
    classe = instance.__class__.__name__
    LOG.debug("{}.post_save()".format(classe))


def pre_delete_me(sender, **kwargs):
    """
    Fonction générique de debug pour tracer les appels.
    """
    instance = kwargs['instance']
    classe = instance.__class__.__name__
    LOG.debug("{}.pre_delete()".format(classe))


def post_delete_me(sender, **kwargs):
    """
    Fonction générique de debug pour tracer les appels.
    """
    instance = kwargs['instance']
    classe = instance.__class__.__name__
    LOG.debug("{}.post_delete()".format(classe))
    if hasattr(instance, 'fichier'):
        LOG.debug("{classe}.fichier.path = {chemin}"
                  .format(classe=classe,
                          chemin=instance.fichier.path))


def lock_delete_Programmation(sender, **kwargs):
    """
    Empêche de supprimer une `Programmation` débutant dans le passé.
    """
    instance = kwargs['instance']
    LOG.debug("Demande de suppression de {instance}"
              .format(instance=instance))
    maintenant = timezone.now()
    limite_debut = maintenant + settings.DELAI_AVANT_DIFFUSION
    if instance.debut <= limite_debut:
        LOG.debug("--> Impossible de supprimer {instance}"
                  .format(instance=instance))
        message = "Une programmation ne peut pas être supprimée si " \
                  "elle a déjà commencé ou si elle va bientôt commencer. " \
                  "La suppression est autorisée pour " \
                  "les programmations débutant après " \
                  "{date}.".format(
                          date=limite_debut.strftime(settings.DATETIME_FORMAT))
        if instance.fin > limite_debut:
            message += "Pour annuler les occurences pas encore " \
                       "diffusées, il faut modifier la date de " \
                       "fin de la programmation."
        raise LockedError(message, instance)
    else:
        LOG.debug("--> Suppression de {instance}."
                  .format(instance=instance))


def lock_delete_Ordonner(sender, **kwargs):
    """
    Lance une LockedError si on essaye de supprimer un `Ordonner` dont
    la `Playlist` fait partie d'une `Programmation`.
    """
    # TODO : voir si cette fonction est bien nécessaire puisque
    # update_duree_Playlist est appelé aussi qui provoque un save() de Playlist
    # et donc l'appel à verification_programmation()
    instance = kwargs['instance']
    try:
        instance.playlist.diffusable_ptr.verification_programmation()
    except LockedError as erreur:
        message = "Impossible de supprimer %s. " % (instance)
        message += erreur.message
        if isinstance(erreur.objects, list):
            related_objects = erreur.objects
        else:
            related_objects = [erreur.objects]
        related_objects.extend(erreur.related_objects)
        raise LockedError(message=message,
                          objects=instance,
                          related_objects=related_objects)


def update_duree_Playlist(sender, **kwargs):
    """
    Actualise la durée d'une `Playlist` après la suppression d'un `Ordonner`.
    """
    instance = kwargs['instance']
    instance.playlist.save()


def deleteImage(sender, **kwargs):
    """
    Supprime une image du système de fichiers après la suppression
    d'un `Album`, d'une `Emission` ou d'un `Episode`.
    """
    instance = kwargs['instance']
    instance.supprimer_image()


"""
Connexion des signaux aux fonctions de callback
"""
# Musiques
post_save.connect(receiver=save_metadata, sender=Album)
post_delete.connect(receiver=deleteImage, sender=Album)
post_save.connect(receiver=save_metadata, sender=Artiste)
post_save.connect(receiver=save_metadata, sender=Genre)
post_save.connect(receiver=save_metadata, sender=Piste)

# Emissions
post_save.connect(receiver=save_metadata, sender=Emission)
post_delete.connect(receiver=deleteImage, sender=Emission)
post_save.connect(receiver=save_metadata, sender=Episode)
post_delete.connect(receiver=deleteImage, sender=Episode)
post_save.connect(receiver=save_metadata, sender=Intervenant)
post_save.connect(receiver=save_metadata, sender=Intervention)
post_delete.connect(receiver=save_metadata, sender=Intervention)
post_save.connect(receiver=save_metadata, sender=Role)

# Diffusable
post_save.connect(receiver=save_metadata, sender=Editeur)
post_save.connect(receiver=save_metadata, sender=Licence)

# Ordonner
pre_delete.connect(receiver=lock_delete_Ordonner, sender=Ordonner)
post_delete.connect(receiver=update_duree_Playlist, sender=Ordonner)

# Programmation
pre_delete.connect(receiver=lock_delete_Programmation, sender=Programmation)
