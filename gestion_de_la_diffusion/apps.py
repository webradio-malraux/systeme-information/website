# -*- coding: utf-8 -*-

"""
Définition de la configuration de l'application `gestion_de_la_diffusion`.
"""

from django.apps import AppConfig


class GestionDeLaDiffusionConfig(AppConfig):
    """
    Configuration de l'application "gestion_de_la_diffusion".
    """
    name = "gestion_de_la_diffusion"
    verbose_name = "gestion de la diffusion du contenu"

    def ready(self):
        import gestion_de_la_diffusion.signals