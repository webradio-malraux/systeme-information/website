# -*- coding: utf-8 -*-

"""
Définition du modèle de données pour la gestion de la diffusion.
"""

import os
import logging
import datetime
import tempfile
from PIL import Image
from django.db import models
from django.db.models import F
from django.utils import timezone
from django.utils.html import format_html, format_html_join
from django.utils.text import get_valid_filename
from django.contrib.auth.models import User
from django.core.exceptions import (ObjectDoesNotExist,
                                    ValidationError)
from website import settings
from .exceptions import LockedError
from .fields import SoundField
from .utils import Occurrence

LOG = logging.getLogger(__name__)


class ObjetImage(models.Model):
    """
    Modèle de `ObjetImage` : classe abstraite qui permet d'affecter une image
    aux `Album`, `Emission` et `Episode`.
    """
    image = models.ImageField(
            "fichier image",
            upload_to=settings.UPLOAD_DIR,
            blank=True,
            null=True,
            help_text="(Facultatif) Tous les formats d'images courant sont"
                      " acceptés. L'image doit être libre de droits.")
    description_image = models.CharField(
            "description de l'image",
            max_length=500,
            blank=True,
            null=True,
            help_text="(Facultatif) Description de l'image : nature, auteur,"
                      "personnes représentées...")

    def display_image_html(self):
        """
        Code html pour afficher l'image en dimension 150x150
        en préservant le ratio.
        """
        image_height = self.image.height
        image_width = self.image.width
        if image_height > image_width:
            display_height = 150
            display_width = 150 * image_width // image_height
        else:
            display_width = 150
            display_height = 150 * image_height // image_width
        return format_html("<img src='{path}' width='{width}' "
                           "height='{height}' >",
                           path=self.image.path,
                           width=display_width,
                           height=display_height)

    display_image_html.short_description = "Image"

    @property
    def image_existe(self):
        """
        Retourne True s'il y a une image dans l'objet
        (en cours de création ou créé).
        """
        return self.image.name != "" and self.image.name is not None

    def traitement_image(self):
        """
        Supprime l'ancien fichier image si besoin.
        Limite la taille de l'image à IMAGE_MAX_SIZE.
        Convertit l'image en jpeg.
        Enregistre le fichier image dans le système de stockage.
        Le nom et le chemin sont différents selon que l'objet est
        un `Album`, une `Emission` ou un `Episode`.
        """
        cls = self.__class__
        # S'il n'y a pas d'image dans l'enregistrement en cours
        # et que le fichier image existe dans le système de stockage,
        # ou si l'image a été modifiée
        # il faut supprimer l'ancien fichier s'il existe
        if self.pk is not None:
            old_obj = cls.objects.get(pk=self.pk)
            if old_obj.image_existe and old_obj.image.name != self.image.name:
                old_path = old_obj.image.path
                if os.path.isfile(old_path):
                    os.remove(old_path)
        image_a_conformer = False
        # S'il y a une nouvelle image (image modifiée ou nouvel enregistrement)
        # il faut conformer l'image en jpeg, de dimension max 1024x1024
        if self.image_existe:
            # Si c'est un nouvel objet
            if self.pk is None:
                image_a_conformer = True
            # Si c'est une image modifiée
            elif self.image.name != cls.objects.get(pk=self.pk).image.name:
                image_a_conformer = True
        if image_a_conformer:
            LOG.debug("Il faut vérifier l'image.")
            image_a_enregistrer = False
            imagePIL = Image.open(self.image.file)
            LOG.debug("Propriétés de l'image : format = {nature}, "
                      "mode = {mode}, dimensions = {largeur}x{hauteur}"
                      .format(nature=imagePIL.format,
                              mode=imagePIL.mode,
                              largeur=imagePIL.width,
                              hauteur=imagePIL.height))
            if imagePIL.format != settings.IMAGE_CONFIG["format"]:
                image_a_enregistrer = True
            if imagePIL.mode is not settings.IMAGE_CONFIG["mode"]:
                image_a_enregistrer = True
                imagePIL = imagePIL.convert(settings.IMAGE_CONFIG["mode"])
            if imagePIL.width >= settings.IMAGE_CONFIG["max_size"][0] \
                    or imagePIL.height >= settings.IMAGE_CONFIG["max_size"][1]:
                image_a_enregistrer = True
                imagePIL.thumbnail(settings.IMAGE_CONFIG["max_size"],
                                   Image.LANCZOS)
            if image_a_enregistrer:
                LOG.debug("Il faut conformer l'image.")
                # Enregistre l'image dans un fichier temporaire
                temp_file = tempfile.TemporaryFile()
                imagePIL.save(temp_file,
                              format=settings.IMAGE_CONFIG["format"],
                              quality=95)
                # Cherche le nom de fichier valide qui sera utilisé pour
                # l'enregistrement : par exemple les "(" sont transformés
                # en "_"
                (old_head, old_name) = os.path.split(self.image.path)
                old_image_path = os.path.join(old_head,
                                              get_valid_filename(old_name))
                self.image.save(self.image.path,
                                temp_file,
                                save=False)
                # Supprime l'ancien fichier si nécessaire
                if old_image_path != self.image.path:
                    if os.path.isfile(old_image_path):
                        os.remove(old_image_path)
                LOG.debug("Image conformée. Nom du fichier : {path}"
                          .format(path=self.image.path))
        # Calcul du chemin du fichier image et du nom du fichier image
        # Si l'image a été conformée, il faut renomer le fichier téléversé
        # Si le nom de l'`Album`, `Episode` ou `Emission` a changé,
        # il faut renommer l'ancien fichier image
        if cls.__name__ == "Album":
            # Chemin relatif à settings.MEDIA_ROOT du nouvel emplacement
            # ex : IMAGE_DIR/album
            save_path = os.path.join(settings.IMAGE_DIR,
                                     "Album")
            # Chemin relatif à settings.MEDIA_ROOT
            # du nouvel emplacement + nom du fichier
            # ex : IMAGE_DIR/Album/nom_album.jpg
            save_filename = os.path.join(
                    save_path,
                    "{nom}.{extension}"
                    .format(nom=str(self),
                            extension=settings.IMAGE_CONFIG["extension"]))
        elif cls.__name__ == "Emission":
            # ex : IMAGE_DIR/Emission
            save_path = os.path.join(settings.IMAGE_DIR,
                                     "Emission")
            # ex : IMAGE_DIR/Emission/nom_emission.jpg
            save_filename = os.path.join(
                    save_path,
                    "{nom}.{extension}"
                    .format(nom=str(self),
                            extension=settings.IMAGE_CONFIG["extension"]))
        elif cls.__name__ == "Episode":
            # ex : IMAGE_DIR/Emission
            save_path = os.path.join(settings.IMAGE_DIR,
                                     "Emission")
            # ex : IMAGE_DIR/Emission/nom_emission-nom_episode.jpg
            save_filename = os.path.join(
                    save_path,
                    "{emission}-{titre}.{extension}"
                    .format(emission=str(self.emission),
                            titre=str(self.titre),
                            extension=settings.IMAGE_CONFIG["extension"]))
        # Chemin absolu du nouvel emplacement
        # ex pour un `Album` : MEDIA_ROOT/IMAGE_DIR/Album
        root_save_path = os.path.join(settings.MEDIA_ROOT,
                                      save_path)
        # Chemin absolu du nouvel emplacement + nom du fichier
        # ex pour un `Album` : MEDIA_ROOT/IMAGE_DIR/Album/nom_album.jpg
        root_save_filename = os.path.join(settings.MEDIA_ROOT,
                                          save_filename)
        # Renomme le fichier si une image a été conformée
        # ou si le nom de l'image a changé
        if self.image_existe:
            if image_a_conformer or self.image.name != save_filename:
                LOG.debug("Il faut renommer l'image.")
                # Crée le chemin s'il n'existe pas
                os.makedirs(root_save_path, exist_ok=True)
                # Chemin absolu du fichier initial + nom du fichier
                root_filename_ini = self.image.path
                self.image.name = save_filename
                os.rename(root_filename_ini, root_save_filename)
                LOG.debug("Image renommée : {old_path} -> {new_path}"
                          .format(old_path=root_filename_ini,
                                  new_path=root_save_filename))

    def supprimer_image(self):
        """
        Supprime le fichier image dans le système de stockage
        s'il existe.
        """
        if self.image_existe:
            root_filename = self.image.path
            if os.path.isfile(root_filename):
                os.remove(root_filename)

    class Meta:
        """
        Meta données de la classe `ObjetImage`.
        """
        abstract = True


class Medium(models.Model):
    """
    Modèle de `Medium` : fournit le medium de provenance d'un `Diffusable`.
    """
    nom = models.CharField(
            max_length=500,
            unique=True,
            help_text="Type de medium permettant de qualifier"
                      " la provenance d'un objet sonore.")

    def __str__(self):
        return "{nom}".format(nom=self.nom)


class Provenance(models.Model):
    """
    Modèle de `Provenance` : fournit la description et l'url du `Medium`.
    L'url et la description ne peuvent pas être nulles ensemble.
    """
    description = models.CharField(
            max_length=500,
            blank=True,
            null=True,
            help_text="Un des deux champs (description ou lien Internet)"
                      " doit être renseigné.")
    url = models.URLField(
            "lien Internet",
            blank=True,
            null=True,
            help_text="À renseigner obligatoirement pour les media "
                      "Internet ou {provenance_interne}."
                      .format(provenance_interne=settings.PROVENANCE_INTERNE))
    medium = models.ForeignKey(
            Medium,
            on_delete=models.PROTECT)

    def __str__(self):
        if self.description is not None:
            return "{description}".format(description=self.description)
        return "{url}".format(url=self.url)

    def validation(self, exclude=None):
        """
        Vérification que pour les media Internet et PROVENANCE_INTERNE, l'url
        est bien renseignée.
        Vérification qu'un des deux champs description et url n'est pas vide.
        """
        try:
            enregistrement_existe = False
            if self.url == "" or self.url is None:
                enregistrement_existe = Provenance.objects.filter(
                                                description=self.description
                                                                ).exists()
            elif self.description == "" or self.description is None:
                enregistrement_existe = Provenance.objects.filter(
                                                url=self.url
                                                                ).exists()
            else:
                enregistrement_existe = Provenance.objects.filter(
                                                description=self.description,
                                                url=self.url
                                                               ).exists()
            if enregistrement_existe:
                msg = "Cet enregistrement existe déjà."
                raise ValidationError(msg)
            if ((self.url is None or self.url == "")
                    and self.medium.nom == settings.PROVENANCE_INTERNE):
                msg = "L'url du site Internet de {provenance_interne}" \
                      " n'a pas été renseignée." \
                      .format(provenance_interne=settings.PROVENANCE_INTERNE)
                raise ValidationError(msg)
            elif (self.url is None or self.url == "") \
                    and self.medium.nom == "Internet":
                msg = "L'url du site Internet de provenance" \
                      " n'a pas été renseignée."
                raise ValidationError(msg)
            elif (self.url is None or self.url == "") \
                    and (self.description is None or self.description == ""):
                msg = "Au moins une descrition ou un lien Internet" \
                      " doivent être renseignés."
                raise ValidationError(msg)
        except Medium.DoesNotExist:
            return

    def save(self, *args, **kwargs):
        self.validation()
        super().save(*args, **kwargs)


class Licence(models.Model):
    """
    Modèle pour la `Licence` associée à un `Diffusable`.
    L'url alimentera le champ ID3v2 : copyright/legal info = WCOP.
    """
    acronyme = models.CharField(
            max_length=50,
            unique=True,
            help_text="Abréviation de la licence.")
    nom = models.CharField(
            max_length=255,
            unique=True,
            help_text="Nom complet de la licence.")
    url = models.URLField(
            "lien Internet",
            blank=True,
            help_text="(Facultatif) Site Internet du créateur de la licence "
                      "(Renseignera le champ WCOP dans le Tag ID3v2 "
                      "du fichier audio).")
    texte = models.TextField(
            "définition de la licence",
            help_text="Texte complet de la licence.")

    def __str__(self):
        return "{acronyme}".format(acronyme=self.acronyme)

    class Meta:
        """
        Meta données de la classe `Licence`.
        """
        ordering = ["acronyme"]


class Editeur(models.Model):
    """
    L'`Editeur` pouvant être associé à un `Diffusable`.
    """
    nom = models.CharField(
            max_length=255,
            unique=True,
            help_text="Nom de l'éditeur (Renseignera le champ"
                      " TPUB dans le Tag ID3v2 du fichier audio).")
    url = models.URLField(
            verbose_name="site officiel",
            help_text="Adresse web du site de l'éditeur. Doit être une adresse"
                      " web valide (Renseignera le champ WPUB dans le"
                      " Tag ID3v2 du fichier audio).")

    def __str__(self):
        return "{nom}".format(nom=self.nom)

    class Meta:
        """
        Meta données de la classe `Editeur`.
        """
        verbose_name = "éditeur"


class ObjetSonore(models.Model):
    """
    Classe permettant de généraliser la gestion du fichier sonore
    d'un `Diffusable`.
    """
    objet_sonore_id = models.AutoField(
            primary_key=True)
    fichier = SoundField(
            "fichier sonore",
            upload_to=settings.UPLOAD_DIR,
            max_length=500,
            help_text="Formats de fichiers autorisés : {liste_format}.".format(
                      liste_format=str(SoundField.available_formats)
                                   .strip("][")))

    def __str__(self):
        try:
            return "{path!r}".format(path=self.fichier.path)
        except ValueError:
            return "Fichier indisponible"

    def fichier_disponible(self):
        try:
            return os.path.isfile(self.fichier.path)
        except ValueError:
            return False

    fichier_disponible.boolean = True

    def save(self, *args, **kwargs):
        LOG.debug("START ObjetSonore.save()")
        super().save(*args, **kwargs)
        if self.fichier_disponible():
            LOG.debug("# Chemin du fichier de l'ObjetSonore {!r}"
                      .format(self.fichier.path))
        LOG.debug("END ObjetSonore.save()")

    def delete(self, save=True, keep_ObjetSonore=False, **kwargs):
        """
        Surcharge la méthode delete pour effectuer les suppressions
        de fichiers nécessaires.
        keep_ObjetSonore: booléen permettant de ne pas appeler
        la destruction de l'`ObjetSonore` associé (dans le cas
        des tests par exemple).
        """
        LOG.debug("START ObjetSonore.delete()")
        LOG.debug("Suppression du fichier associé à l'ObjetSonore {!r}"
                  .format(self))
        try:
            LOG.debug("  ↳ {!r}".format(self.fichier.path))
            self.fichier.delete(save=False, keep_file=keep_ObjetSonore)
        except ValueError:
            LOG.info("Fichier indisponible sur le système de fichier")
        super().delete(**kwargs)
        LOG.debug("END ObjetSonore.delete()")


class Diffusable(models.Model):
    """
    Un `Diffusable` est la classe mère de tout objet diffusable à la radio.
    """
    titre = models.CharField(
            max_length=255)
    date_publication = models.DateField(
            "date de publication",
            help_text="(Renseigne le champ TYER dans le Tag "
                      "ID3v2 du fichier audio).")
    licence = models.ForeignKey(
            Licence,
            on_delete=models.PROTECT,
            default=6,
            help_text="(Renseigne le champ WCOP dans le Tag "
                      "ID3v2 du fichier audio).")
    editeur = models.ForeignKey(
            Editeur,
            on_delete=models.PROTECT,
            verbose_name="éditeur",
            help_text="(Renseigne le champ TPUB ou WPUB dans le Tag "
                      "ID3v2 du fichier audio).")
    _duree = models.DurationField(
            "durée",
            editable=False,
            default=settings.DUREE_NULLE)

    def __str__(self):
        return "{titre}".format(titre=self.titre)

    @property
    def duree(self):
        return self._duree

    @duree.setter
    def duree(self, value):
        if type(value) is int or type(value) is float:
            if value >= 0:
                self._duree = datetime.timedelta(seconds=int(value))
            else:
                self._duree = settings.DUREE_NULLE
        elif type(value) is datetime.timedelta:
            self._duree = value
        else:
            raise TypeError("La durée ne peut être fournie qu'en : "
                            "'datetime.timedelta', "
                            "'int' "
                            "ou 'float'")

    @duree.deleter
    def duree(self):
        raise NotImplementedError()

    @property
    def nature(self):
        """
        Retourne la sous-classe de l'objet `Diffusable`.
        """
        nature_str = "inconnue"
        for nature_possible in ["episode", "piste", "playlist"]:
            try:
                nature_str = getattr(self, nature_possible)._meta.verbose_name
            except ObjectDoesNotExist:
                pass
        return nature_str

    def est_programme(self):
        """
        Retourne True si le `Diffusable` fait partie d'une `Programmation`
        pour une diffusion de lui-même ou dans une `Playlist`.
        """
        if self.pk is not None:
            if Programmation.objects.filter(diffusable__pk=self.pk).exists():
                return True
            playlists = self.playlist_set_liste.all()
            if playlists.exists():
                for playlist in playlists:
                    if playlist.est_programme():
                        return True
        return False

    est_programme.boolean = True

    est_programme.short_description = "est programmé"

    def programmations(self):
        """
        Renvoie la liste des noms des `Programmation`
        dont le `Diffusable` fait partie.
        """
        programmations = []
        if self.pk is not None:
            for prog in Programmation.objects.filter(diffusable__pk=self.pk):
                programmations.append(str(prog))
            playlists = self.playlist_set_liste.all()
            for playlist in playlists:
                programmations.extend(playlist.programmations())
        return programmations

    def dates_programmations(self):
        """
        Renvoie la liste des datetimes du début de chaque `Occurence`
        des `Programmation` dont le `Diffusable` fait partie (`ObjetSonore`
        ou `Playlist`).
        Pour un `Diffusable` faisant partie d'une `Playlist` le datetime
        correspond au début de la `Programmation` de la `Playlist` et non
        au début de la diffusion du `Diffusable`.
        """
        dates_programmations = []
        if self.pk is not None:
            for prog in Programmation.objects.filter(diffusable__pk=self.pk):
                for occ in prog.get_occurrences():
                    dates_programmations.append(occ.debut)
            playlists = self.playlist_set_liste.all()
            for playlist in playlists:
                dates_programmations.extend(playlist.dates_programmations())
        return dates_programmations

    def programmations_liste_html(self):
        """
        Retourne le code html de la liste des débuts des`Occurence`
        des `Programmation` du `Diffusable`.
        """
        programmations = self.dates_programmations()
        liste_html = format_html_join(
                "\n",
                "le <strong>{}</strong> à <strong>{}</strong>\n<br/>",
                ((debut.strftime("%A %d %B %Y"),
                  debut.strftime("%H h %M"))
                 for debut in programmations),)
        return format_html("\n{}\n",
                           liste_html)

    programmations_liste_html.short_description = "Diffusions programmées"

    def verification_programmation(self):
        """
        Empêche la modification d'un `Diffusable` faisant
        partie d'une `Programmation`.
        """
        progs = self.programmations()
        if len(progs) != 0:
            diffusable_initial = Diffusable.objects.get(pk=self.pk)
            if len(progs) == 1:
                message = "'{titre}' ne peut plus être modifié " \
                          "car il est verrouillé par une programmation" \
                          .format(titre=diffusable_initial.titre)
            else:
                message = "'{titre}' ne peut plus être modifié car il est" \
                          " verrouillé par {nb_progs} programmation(s)" \
                          .format(titre=diffusable_initial.titre,
                                  nb_progs=len(progs))
            raise LockedError(message=message,
                              objects=diffusable_initial,
                              related_objects=progs)

    def playlists_liste_html(self):
        """
        Retourne le code html de la liste des `Playlist` contenant
        le `Diffusable`.
        """
        ordonners = self.ordonner_set_diffusable.all()
        liste_html = format_html_join(
                "\n",
                "<strong>{}</strong> en position "
                "<strong>{}</strong>\n<br/>",
                ((ordonner.playlist, ordonner.position)
                 for ordonner in ordonners))
        return format_html("\n{}\n",
                           liste_html)

    playlists_liste_html.short_description = "Listes de lecture"

    def save(self, *args, **kwargs):
        LOG.debug("START Diffusable.save()")
        self.verification_programmation()
        super().save(*args, **kwargs)
        LOG.debug("END Diffusable.save()")

    class Meta:
        """
        Meta données de la classe `Diffusable`.
        """
        ordering = ["titre"]


class Playlist(Diffusable):
    """
    Une `Playlist` est une collection de `Diffusable` pouvant être diffusée
    La `Playlist` porte la relation vers le `Diffusable`.
    """
    description = models.TextField(
            help_text="Description du contenu de la liste de lecture.")
    liste = models.ManyToManyField(
            Diffusable,
            through="Ordonner",
            related_name="playlist_set_liste",
            help_text="Pistes ou épisodes faisant partie de cette"
                      " liste de lecture.")

    def validation(self, exclude=None):
        """
        Deux `Playlist` ne doivent pas avoir le même titre.
        Vérification de l'unicité du titre pour une `Playlist`.
        """
        try:
            liste_doublons = Playlist.objects.filter(titre=self.titre)
            # Il faut vérifier si la `Playlist` existe déjà
            # ou si c'est une nouvelle
            # Si elle existe déjà, il faut pouvoir modifier les autres champs
            if self.pk is None:
                playlist_existe = liste_doublons.exists()
            else:
                playlist_existe = liste_doublons.exclude(pk=self.pk).exists()
            if playlist_existe:
                message = "Une liste de lecture nommée {titre} existe déjà" \
                           .format(titre=self.titre)
                raise ValidationError(message)
        except Playlist.DoesNotExist:
            return

    @property
    def duree(self):
        """
        Obtient et retourne la durée de la `Playlist` par le sommage de chacun
        des `Diffusable` de sa liste.
        """
        self.update_duree()
        return super().duree

    def update_duree(self):
        """
        Met à jour la durée de la `Playlist` en se basant sur les durées
        de chacun de ses éléments.
        """
        somme = settings.DUREE_NULLE
        if self.id is not None:
            for diffusable in self.liste.all():
                somme += diffusable.duree
            self._duree = somme

    def save(self, *args, **kwargs):
        self.validation()
        self.update_duree()
        super().save(*args, **kwargs)

    class Meta:
        """
        Meta données de la classe `Playlist`.
        """
        verbose_name = "liste de lecture"
        verbose_name_plural = "listes de lecture"


class Ordonner(models.Model):
    """
    La classe `Ordonner` assure le positionnement d'un
    `Diffusable` dans une `Playlist`.
    """
    playlist = models.ForeignKey(
            Playlist,
            on_delete=models.CASCADE,
            verbose_name="liste de lecture",
            related_name="ordonner_set_playlist")
    diffusable = models.ForeignKey(
            Diffusable,
            on_delete=models.CASCADE,
            verbose_name="diffusable",
            related_name="ordonner_set_diffusable")
    position = models.PositiveIntegerField(
            help_text="Position du diffusable dans la liste de lecture. "
                      "Les positions doivent être toutes différentes "
                      "dans une même liste de lecture.")

    def __str__(self):
        return "{position} - {titre} dans la playlist {playlist}" \
               .format(position=str(self.position),
                       titre=str(self.diffusable),
                       playlist=str(self.playlist))

    def validation(self):
        """
        Une `Playlist` ne doit pas se contenir elle-même
        et plus généralement, il faut empêcher toute référence circulaire
        """
        def exists_reference_circulaire(playlist_a_chercher, playlist):
            """
            Parcourt les `Diffusable` de playlist
            S'il y a une `Playlist` parmi eux :
                si c'est la playlist_a_chercher :
                    retourne True
                si c'est une autre `Playlist` :
                    recherche une référence circulaire
            Sinon retourne False
            """
            for diffusable in playlist.liste.all():
                if diffusable.nature == "liste de lecture":
                    if diffusable.pk == playlist_a_chercher.pk:
                        return True
                    else:
                        if exists_reference_circulaire(playlist_a_chercher,
                                                       diffusable.playlist):
                            return True
            return False

        # Vérifie que la `Playlist` peut être modifiée (car non programmée)
        self.playlist.diffusable_ptr.verification_programmation()
        # Vérifie que l'`Ordonner` ne crée pas une référence circulaire
        try:
            if self.diffusable.nature == "liste de lecture":
                playlist_a_chercher = self.playlist
                playlist = self.diffusable.playlist
                message = "Une liste de lecture ne peut pas se "\
                          "contenir elle-même"
                if playlist_a_chercher.pk == playlist.pk:
                    raise ValidationError(message)
                elif exists_reference_circulaire(playlist_a_chercher,
                                                 playlist):
                    raise ValidationError(message)
        except Playlist.DoesNotExist:
            return

    def save(self, *args, **kwargs):
        self.validation()
        super().save(*args, **kwargs)
        self.playlist.save()

    class Meta:
        """
        Meta données de la classe `Ordonner`.
        """
        verbose_name = "contenu"
        unique_together = ("playlist", "position")


class Frequence(models.Model):
    """
    Modèle de `Frequence` :
    Fréquences possibles pour la répétition d'`Occurrence`
    dans une `Programmation`.
    """
    nom = models.CharField(
            max_length=50,
            unique=True)
    delta = models.DurationField(
            "durée entre deux occurrences",
            help_text="Le format est 'j hh:mm:ss'",
            unique=True)

    def __str__(self):
        return "{nom}".format(nom=self.nom)


class ProgrammationManager(models.Manager):
    def occurrences_set(self, debut=None, fin=None, pk_exclude=None):
        """
        Programmation.objects.occurrences_set(debut, fin) retourne l'ensemble
        des `Occurrence` programmées dont au moins une partie se situe
        entre debut et fin (strictement).
        Une `Occurrence` est représentée par un tuple :
            (date de début, date de fin).
        Le paramètre pk_exclude premet d'exclure de la recherche une
        `Programmation` en attribuant à pk_exclude la valeur de sa primaryKey.
        Si debut=None alors debut=now().
        Si fin=None alors fin=fin de l'année scolaire (31/08 minuit), l'année
        scolaire commençant le 01/07 minuit.
        La fonction sera à adapter pour pouvoir retourner une liste
        d'`Occurrence` au lecteur ou à un agenda.
        """
        # Initialise les valeurs de debut et fin
        # si elles n'ont pas été renseignées
        maintenant = timezone.now()
        if debut is None:
            debut = maintenant
        if fin is None:
            annee_fin = (maintenant + settings.DUREE_JUILLET_DECEMBRE).year
            fin = timezone.make_aware(datetime.datetime(day=1,
                                                        month=9,
                                                        year=annee_fin))
        # Les `Programmation` susceptibles de contenir les `Occurrence`
        # cherchées ont un debut inférieur à la fin de la recherche
        # et une fin supérieure au debut de la recherche
        # (fin est en fait la date limite du début de l'`Occurrence` plus la
        # durée du `Diffusable`)
        programmations = Programmation.objects.filter(
                                    debut__lte=fin,
                                    diffusable___duree__gte=debut-F("fin"))
        if pk_exclude is not None:
            if Programmation.objects.filter(pk=pk_exclude).exists():
                programmations = programmations.exclude(pk=pk_exclude)
        occurrences = []
        for prog in programmations:
            prog_freq = prog.frequence.delta
            prog_duree = prog.diffusable.duree
            prog_debut = prog.debut  # début de la première `Occurrence`
            prog_fin = prog.fin_reelle()  # fin de la dernière `Occurrence`
            # Teste si les `Occurrence` de la `Programmation`
            # sont susceptibles d'être entre debut et fin
            if debut <= prog_debut < fin \
                    or debut < prog_fin <= fin \
                    or (debut > prog_debut and prog_fin > fin):
                # s'il n'y a pas de répétition
                # ou si le début de la `Programmation` est entre debut et fin,
                # on est sûr que la première `Occurrence` est dans la plage
                if prog_freq == settings.DUREE_NULLE or prog_debut >= debut:
                    occ_debut = prog_debut
                    occ_fin = occ_debut + prog_duree
                    occurrences.append(Occurrence(occ_debut, occ_fin))
                else:
                    # Si le début de la `Programmation` est avant debut,
                    # on cherche la première `Occurrence` qui débute
                    # avant debut
                    num_debut = (debut - prog_debut) // prog_freq
                    occ_debut = prog_debut + num_debut*prog_freq
                    # Vérifie si l'`Occurrence` finit bien après debut
                    occ_fin = occ_debut + prog_duree
                    if occ_fin > debut:
                        occurrences.append(Occurrence(occ_debut, occ_fin))
                # S'il y a répétition, on ajoute toutes les `Occurrence`
                # suivantes jusqu'à atteindre fin
                if prog_freq > settings.DUREE_NULLE:
                    occ_debut += prog_freq
                    while occ_debut <= fin and occ_debut <= prog.fin:
                        occ_fin += prog_freq
                        occurrences.append(Occurrence(occ_debut, occ_fin))
                        occ_debut += prog_freq
        return occurrences


class Programmation(models.Model):
    """
    Modèle de `Programmation` :
    Permet de programmer la diffusion d'un `Diffusable` à partir d'une date
    avec une certaine `Frequence`.
    """
    debut = models.DateTimeField(
            "début",
            help_text="Date et heure du début de la programmation. Ne peut "
                      "pas être créée dans le passé.")
    fin = models.DateTimeField(
            help_text="Date et heure limite du début de la dernière"
                      " occurrence de la programmation.")
    frequence = models.ForeignKey(
            Frequence,
            on_delete=models.PROTECT,
            verbose_name="fréquence")
    diffusable = models.ForeignKey(
            Diffusable,
            on_delete=models.PROTECT)
    programmateur = models.ForeignKey(
            User,
            on_delete=models.PROTECT)
    date_creation = models.DateTimeField(
            "date de création",
            auto_now_add=True)
    date_modification = models.DateTimeField(
            "date de modification",
            auto_now=True)
    objects = ProgrammationManager()

    def __str__(self):
        return "{titre} programmé {frequence} du {debut} au {fin}" \
               .format(titre=str(self.diffusable),
                       frequence=str(self.frequence).lower(),
                       debut=self.debut.strftime(settings.DATETIME_FORMAT),
                       fin=self.fin.strftime(settings.DATETIME_FORMAT))

    def get_occurrences(self):
        """
        Renvoie toutes les `Occurrence` associées à la `Programmation`.
        """
        occ_debut = self.debut
        occ_duree = self.diffusable.duree
        occ_fin = occ_debut + occ_duree
        freq = self.frequence.delta
        occurrences = [Occurrence(occ_debut, occ_debut + occ_duree)]
        if freq > settings.DUREE_NULLE:
            occ_debut += freq
            while occ_debut <= self.fin:
                occ_fin += freq
                occurrences.append(Occurrence(occ_debut, occ_fin))
                occ_debut += freq
        return occurrences

    def occurrences_liste_html(self):
        """
        Retourne le code html de la liste des débuts des `Occurence`
        d'une `Programmation`.
        """
        liste_html = format_html_join(
                "\n",
                "le <strong>{}</strong> à <strong>{}</strong>\n<br/>",
                ((occ.debut.strftime("%A %d %B %Y"),
                  occ.debut.strftime("%H h %M"))
                 for occ in self.get_occurrences()),)
        return format_html("\n{}\n",
                           liste_html)

    occurrences_liste_html.short_description = "Occurrences"

    def fin_reelle(self):
        """
        Retourne le datetime de la fin de la dernière `Occurrence` de la
        `Programmation`, (la fin d'une `Programmation` ne correspond pas
        nécessairement au début de la dernière `Occurrence`).
        """
        duree = self.diffusable._duree
        if self.frequence.delta == settings.DUREE_NULLE:
            fin = self.debut + duree
        else:
            num_fin = (self.fin - self.debut) // self.frequence.delta
            fin = self.debut + num_fin*self.frequence.delta + duree
        return fin

    def validation(self):
        """
        Vérifie qu'on n'a pas modifié une `Programmation` déjà existante.
        Vérifie que debut n'est pas dans le passé.
        Vérifie que fin est après debut.
        Vérifie que fin est dans l'année scolaire.
        Vérifie que le nombre d'`Occurrence` est inférieur à MAX_REPETITIONS.
        Vérifie que la durée du `Diffusable` est inférieure à la `Frequence`.
        Vérifie que tous les créneaux sont libres.
        """
        maintenant = timezone.now()
        # DELAI_AVANT_DIFFUSION est le délai avant lequel
        # aucunne modification n'est possible
        date_limite = maintenant + settings.DELAI_AVANT_DIFFUSION
        date_limite_str = date_limite.strftime(settings.DATETIME_FORMAT)
        # Vérifications spécifiques à la modification
        # d'une `Programmation` déjà existante
        if self.pk is not None:
            prog_initiale = Programmation.objects.get(pk=self.pk)
            # Si le début de la `Programmation` était passée,
            # on ne peut rien modifier sauf la fin si elle était dans le
            # futur et qu'on la modifie dans le futur en respectant
            # le délai DELAI_AVANT_DIFFUSION
            if prog_initiale.debut < date_limite:
                if prog_initiale.debut != self.debut:
                    message = "Impossible de modifier le début " \
                              "d'une programmation commençant " \
                              "avant le {date}.".format(date=date_limite_str)
                    raise ValidationError(message)
                elif prog_initiale.diffusable != self.diffusable:
                    message = "Impossible de modifier le diffusable " \
                              "d'une programmation commençant " \
                              "avant le {date}.".format(date=date_limite_str)
                    raise ValidationError(message)
                elif prog_initiale.frequence != self.frequence:
                    message = "Impossible de modifier la fréquence " \
                              "d'une programmation commençant " \
                              "avant le {date}.".format(date=date_limite_str)
                    raise ValidationError(message)
                elif prog_initiale.programmateur != self.programmateur:
                    message = "Impossible de modifier le programmateur " \
                              "d'une programmation commençant " \
                              "avant le {date}.".format(date=date_limite_str)
                    raise ValidationError(message)
                elif prog_initiale.fin != self.fin:
                    if prog_initiale.fin < date_limite:
                        message = "Impossible de modifier la fin " \
                                  "d'une programmation terminant avant " \
                                  "le {date}.".format(date=date_limite_str)
                        raise ValidationError(message)
                    elif self.fin < date_limite:
                        message = "La date limite pour modifier la fin " \
                                  "d'une programmation déja commencée " \
                                  "est {date}.".format(date=date_limite_str)
                        raise ValidationError(message)
            # Si le début de la `Programmation` était futur,
            # on vérifie qu'elle n'a pas été mise dans le passé
            elif self.debut < date_limite:
                message = "La date limite pour modifier le début " \
                          "d'une programmation " \
                          "est {date}.".format(date=date_limite_str)
                raise ValidationError(message)
        # Vérification spécifique à la création d'une `Programmation`
        else:
            # Vérification que debut n'est pas dans le passé
            if self.debut < date_limite:
                message = "Une programmation ne peut pas être créée " \
                          "avant le {date}.".format(date=date_limite_str)
                raise ValidationError(message)
        # Vérifications communes à une modification ou à une création
        # Vérification que fin est après debut
        if self.debut > self.fin:
            message = "La fin de la programmation ne peut pas être " \
                      "antérieure à son début."
            raise ValidationError(message)
        # Vérification que fin est dans l'année scolaire
        annee_fin = (maintenant + settings.DUREE_JUILLET_DECEMBRE).year
        limite_fin = timezone.make_aware(datetime.datetime(day=1,
                                                           month=9,
                                                           year=annee_fin))
        if self.fin_reelle() >= limite_fin:
            message = "La fin de la programmation ne peut pas être " \
                      "postérieure à la fin de l'année scolaire, fixée " \
                      "au 31 août {annee} minuit.".format(annee=annee_fin)
            raise ValidationError(message)
        # Vérification que le nombre d'`Occurrence` est
        # inférieur à MAX_REPETITIONS
        # et que la durée du `Diffusable` est inférieure à `Frequence`
        # On ne teste pas si l'`Occurrence` est unique
        if self.frequence.delta > settings.DUREE_NULLE:
            nb_repetitions = (self.fin - self.debut) // self.frequence.delta
            nb_repetitions = nb_repetitions + 1
            if nb_repetitions > settings.MAX_REPETITIONS:
                message = "Cette programmation comporte {nb_occ} " \
                          "occurrences. Le nombre maximum d'occurrences " \
                          "autorisé est {nb_max}." \
                          .format(nb_occ=nb_repetitions,
                                  nb_max=settings.MAX_REPETITIONS)
                raise ValidationError(message)
            if self.diffusable._duree > self.frequence.delta:
                message = "La durée du diffusable ne peut pas être " \
                          "supérieure à la fréquence de programmation."
                raise ValidationError(message)
        # Vérification que tous les créneaux sont libres
        # Il y a une requète effectuée pour chaque `Occurrence`
        # mais le nombre d'`Occurrence` étant faible (< MAX_REPETITIONS)
        # cela ne pose pas de problème
        occs_a_tester = self.get_occurrences()
        for occ in occs_a_tester:
            programmations = Programmation.objects.occurrences_set(
                                                        debut=occ.debut,
                                                        fin=occ.fin,
                                                        pk_exclude=self.pk)
            if len(programmations) != 0:
                debut_occ = occ.debut.strftime(settings.DATETIME_FORMAT)
                fin_occ = occ.fin.strftime(settings.DATETIME_FORMAT)
                message = "Une programmation déja enregistrée utilise " \
                          "tout ou une partie de la plage horaire " \
                          "({debut}, {fin}).".format(debut=debut_occ,
                                                     fin=fin_occ)
                raise ValidationError(message)

    def save(self, *args, **kwargs):
        self.validation()
        super().save(*args, **kwargs)
