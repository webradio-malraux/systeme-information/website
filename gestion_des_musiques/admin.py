# -*- coding: utf-8 -*-

"""
Définition de la configuration du site d'administration
pour la gestion des musiques.
"""

from django.contrib import admin, messages
from django.utils.safestring import mark_safe
from django.utils.html import escape
from django.urls import reverse
from django.http import HttpResponseRedirect
from .models import (Piste,
                     Album,
                     Artiste,
                     Genre)
from . import forms
from gestion_de_la_diffusion.exceptions import LockedError


class PisteInline(admin.StackedInline):
    """
    Définition des attributs d'affichage Inline
    de la classe `Piste`.
    """
    model = Piste
    fields = [("position", "titre", "fichier", "artiste", "genre"),
              ("date_publication", "licence", "editeur", "provenance")]
    extra = 0
    autocomplete_fields = ["editeur",
                           "artiste",
                           "genre",
                           "provenance"]


class PisteAdmin(admin.ModelAdmin):
    """
    Définition des attributs d'administration
    de la classe `Piste`.
    """
    fieldsets = [(None,
                  {"fields": (("position", "titre"),
                              "fichier",
                              "duree",
                              "album",
                              "date_publication",
                              "artiste",
                              ("licence", "editeur"),
                              ("genre", "provenance"))}),
                 ("DATES DE PROGRAMMATION",
                  {"fields": ("programmations_liste_html",),
                   "classes": ("collapse",)})]
    list_display = ("position",
                    "titre",
                    "duree",
                    "album",
                    "date_publication",
                    "licence",
                    "genre",
                    "fichier_disponible",
                    "est_programme")
    readonly_fields = ["est_programme",
                       "duree",
                       "programmations_liste_html"]
    list_display_links = ("titre",)
    list_filter = ["date_publication",
                   "album",
                   "licence"]
    date_hierarchy = "date_publication"
    search_fields = ["titre",
                     "album__nom"]
    form = forms.PisteForm
    autocomplete_fields = ["editeur",
                           "artiste",
                           "album",
                           "genre",
                           "provenance"]

    def change_view(self, request, object_id, form_url="", extra_context=None):
        """
        Gestion de `LockedError` si on essaye de modifier une `Piste`
        qui est programmée : un message d'erreur est envoyé à l'utilisateur
        indiquant les `Programmation` verrouillant la `Piste`.
        """
        try:
            return super().change_view(request,
                                       object_id,
                                       form_url,
                                       extra_context)
        except LockedError as erreur:
            message = escape(erreur.message)
            for obj in erreur.related_objects:
                message += "<br /><strong> {related_obj} </strong>" \
                           .format(related_obj=escape(obj))
            self.message_user(request, mark_safe(message), messages.ERROR)
            opts = self.model._meta
            return_url = reverse("admin:{app}_{model}_change".format(
                                                     app=opts.app_label,
                                                     model=opts.model_name),
                                 args=(object_id,),
                                 current_app=self.admin_site.name)
            return HttpResponseRedirect(return_url)


class AlbumAdmin(admin.ModelAdmin):
    """
    Définition des attributs d'administration
    de la classe `Album`.
    """
    fields = ["nom",
              "duree",
              "display_image_html",
              "image"]
    readonly_fields = ["duree",
                       "display_image_html"]
    inlines = [PisteInline]
    list_display = ("nom",
                    "duree")
    search_fields = ["nom"]


class GenreAdmin(admin.ModelAdmin):
    """
    Définition des attributs d'administration
    de la classe `Genre`.
    """
    list_display = ("id",
                    "nom",
                    "nombre_de_pistes")
    list_display_links = ("nom",)
    search_fields = ["nom"]


class ArtisteAdmin(admin.ModelAdmin):
    """
    Définition des attributs d'administration
    de la classe `Artiste`.
    """
    search_fields = ["nom"]


admin.site.register(Piste, PisteAdmin)
admin.site.register(Album, AlbumAdmin)
admin.site.register(Genre, GenreAdmin)
admin.site.register(Artiste, ArtisteAdmin)
