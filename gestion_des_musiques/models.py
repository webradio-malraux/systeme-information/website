# -*- coding: utf-8 -*-

"""
Définition du modèle de données pour la gestion des musiques.
"""

import logging
from django.db import models
from gestion_de_la_diffusion.models import (Diffusable,
                                            ObjetSonore,
                                            Provenance,
                                            ObjetImage)
from website import settings

LOG = logging.getLogger(__name__)


class Genre(models.Model):
    """
    Le `Genre` de la musique alimentera le champ correspondant
    en ID3v2 : content type = TCON.
    """
    nom = models.CharField(
            max_length=500,
            unique=True,
            help_text="Genre de la musique (Renseignera le champ "
                      "TCON dans le Tag ID3v2 du fichier audio).")

    def __str__(self):
        return "{nom}".format(nom=self.nom)

    @property
    def nombre_de_pistes(self):
        """
        Retourne le nombre de `Piste` associées à ce `Genre`.
        """
        return self.piste_set.count()

    class Meta:
        """
        Meta données de la classe `Genre`.
        """
        ordering = ['nom']


class Artiste(models.Model):
    """
    Le nom complet de l'`Artiste`.
    Le nom de l'`Artiste` alimentera le champ ID3v2: TPE1.
    """
    nom = models.CharField(
            max_length=255,
            unique=True,
            help_text="Nom de l'artiste (Renseignera le champ "
                      "TPE1 dans le Tag ID3v2 du fichier audio).")

    def __str__(self):
        return "{nom}".format(nom=self.nom)


class Album(ObjetImage):
    """
    Un `Album` représente une collection de `Piste`.
    Le nom de l'`Album` alimentera le champ ID3v2: TALB.
    """
    nom = models.CharField(
            max_length=255,
            unique=True,
            help_text="Nom de l'album (Renseignera le champ "
                      "TALB dans le Tag ID3v2 du fichier audio).")

    @property
    def duree(self):
        """
        Retourne la durée totale de l'`Album` dont les `Piste` sont
        disponibles sur le système de fichiers.
        """
        duree = settings.DUREE_NULLE
        for piste in self.piste_set.all():
            if piste.fichier_disponible():
                duree += piste.duree
        return duree

    def __str__(self):
        return "{nom}".format(nom=self.nom)

    def save(self, *args, **kwargs):
        self.traitement_image()
        super().save(*args, **kwargs)


class Piste(Diffusable, ObjetSonore):
    """
    Une `Piste` est un `Diffusable` ayant du contenu musical
    qui lui est propre.
    """
    position = models.PositiveIntegerField(
            help_text="Position de la piste dans l'album (doit être un "
                      "entier positif, différent des positions "
                      "des autres pistes de l'album).")
    artiste = models.ForeignKey(
            Artiste,
            on_delete=models.PROTECT,
            help_text="Artiste principal (Renseigne "
                      "le champ TPE1 dans le Tag ID3v2 du fichier audio).")
    album = models.ForeignKey(
            Album,
            on_delete=models.PROTECT,
            help_text="Nom de l'album (Renseigne le champ "
                      "TALB dans le Tag ID3v2 du fichier audio).")
    genre = models.ForeignKey(
            Genre,
            on_delete=models.PROTECT,
            help_text="Genre de la musique (Renseigne le champ "
                      "TCON dans le Tag ID3v2 du fichier audio).")
    provenance = models.ForeignKey(
            Provenance,
            on_delete=models.PROTECT)

    def save(self, *args, **kwargs):
        LOG.debug("START Piste.save()")
        super().save(*args, **kwargs)
        LOG.debug("END Piste.save()")

    def delete(self, *args, keep_ObjetSonore=False, **kwargs):
        """
        # TODO: faire en sorte que l'algorithme n'existe que dans le
        parent `ObjetSonore`.

        keep_ObjetSonore : booléen permettant de ne pas appeler la
        destruction de l'`ObjetSonore` associé (lors des tests en particulier).
        """
        LOG.debug("START Piste.delete()")
        if not keep_ObjetSonore:
            super().delete(self, *args, **kwargs)
        else:
            Diffusable.delete(self, *args, **kwargs)

        LOG.debug("END Piste.delete()")

    class Meta:
        """
        Meta données de la classe `Piste`.
        """
        unique_together = ("album", "position")
        ordering = ['album', 'position']
