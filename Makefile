AUTHENTIFICATION := auth/
AUTHENTIFICATION_OBJ := User Group
AUTHENTIFICATION_FIXTURES := $(addsuffix .json,$(addprefix $(AUTHENTIFICATION)fixtures/, $(AUTHENTIFICATION_OBJ)))

GESTION_DE_LA_DIFFUSION := gestion_de_la_diffusion/
GESTION_DE_LA_DIFFUSION_OBJ := Licence Medium Provenance Editeur Diffusable ObjetSonore Playlist Ordonner  Frequence Programmation
GESTION_DE_LA_DIFFUSION_FIXTURES := $(addsuffix .json,$(addprefix $(GESTION_DE_LA_DIFFUSION)fixtures/, $(GESTION_DE_LA_DIFFUSION_OBJ)))

GESTION_DES_MUSIQUES := gestion_des_musiques/
GESTION_DES_MUSIQUES_OBJ := Album Artiste Genre Piste
GESTION_DES_MUSIQUES_FIXTURES := $(addsuffix .json,$(addprefix $(GESTION_DES_MUSIQUES)fixtures/, $(GESTION_DES_MUSIQUES_OBJ)))

GESTION_DES_EMISSIONS := gestion_des_emissions/
GESTION_DES_EMISSIONS_OBJ := Intervenant Role Emission Episode Intervention SourceExterne RelationEpisodeSourceExterne MotClef RelationEpisodeMotClef
GESTION_DES_EMISSIONS_FIXTURES := $(addsuffix .json,$(addprefix $(GESTION_DES_EMISSIONS)fixtures/, $(GESTION_DES_EMISSIONS_OBJ)))

FIXTURES := $(AUTHENTIFICATION_FIXTURES) $(GESTION_DE_LA_DIFFUSION_FIXTURES) $(GESTION_DES_MUSIQUES_FIXTURES) $(GESTION_DES_EMISSIONS_FIXTURES)
OBJS := $(AUTHENTIFICATION_FIXTURES) $(GESTION_DE_LA_DIFFUSION_OBJ) $(GESTION_DES_MUSIQUES_OBJ) $(GESTION_DES_EMISSIONS_OBJ)

.PHONY: all run dumpdata loaddata clean-fixtures clean-deep restart install fresh migrations migrate superuser test $(FIXTURES)

all: run
run:
	./manage.py runserver

clean-fixtures:
	$(warning Suppression des fixtures $(FIXTURES))
	-rm $(FIXTURES)
clean-deep:
	$(warning Suppression de la base de données SQLite et des migrations)
	-rm -rf db.sqlite3 */migrations/00*.py */migrations/__pycache__

install: install-debian-pkgs restart

install-debian-pkgs:
	sudo apt update && sudo apt install audiotools

restart: clean-deep fresh

fresh: migrations migrate
migrations:
	@./manage.py makemigrations
migrate:
	@./manage.py migrate
superuser:
	@./manage.py createsuperuser --username "admin" --email "admin@radio.lycee-malraux-72.net"

dumpdata: $(FIXTURES)
$(FIXTURES):
	$(info Dump des données dans la fixture $@)
	@./manage.py dumpdata $(subst .json, ,$(subst /fixtures/,.,$@)) | json_pp -json_opt utf8,pretty,canonical > $@
loaddata:
	./manage.py loaddata $(OBJS)

test:
	@./manage.py test --no-input --verbosity=2

venv:
	@virtualenv --python python3 --no-site-packages venv
requirements: venv
	@pip3 install -r requirements.txt
venv-clean:
	-rm -rf venv/

.PHONY: docs
docs:
	$(MAKE) -C docs
clean-docs:
	$(MAKE) -C docs clean

shell dbshell showmigrations sqlmigrate sqlflush flush:
	@./manage.py $@
